# Tet2Mesh
Second order tetrahedral mesh class.
Used primarily for CutFEM.


- Generate a 3D domain of P2 tetrahedral elements.
- Fishbone mesh
- Base functions
- CutP2
- CutP1 - Since the edges are straight