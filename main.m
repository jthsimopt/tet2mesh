clear
clc
close all

%% Required files
addpath(fullfile(pwd,'Tet2Mesh'))


%%
x0 = 0; x1 = 1;
y0 = 0; y1 = 1;
z0 = 0; z1 = 1;
ne = 1;
nxe = ne; nye = ne; nze = ne;

tic
mesh = Tet2Mesh(x0,x1,y0,y1,z0,z1,nxe,nye,nze)
toc

xfigure
h = vizP1Mesh(mesh.T(:,1:4),mesh.P); view(3)
title('P1 elements')
% 
xfigure;
h = vizP2Faces(mesh.faces,mesh.P,2,'EdgeLineWidth',1,'Light','true'); view(3)
title('P2 elements')


