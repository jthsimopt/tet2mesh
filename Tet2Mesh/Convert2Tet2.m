function [tet2,P2,Elements] = Convert2Tet2(tet,P)
    
    
    
    
    TR = triangulation(tet,P);
%     neighs = TR.neighbors;
%     edges = TR.edges;
    nele = size(tet,1);
    nnod = size(P,1);
%     nmax = nnod;

    element.edges = zeros(6,3);
    element.faces = zeros(4,6);
    element.neighs = zeros(1,4);
    Elements = repmat(element,nele,1); 
    
%     P2 = NaN(10*nele,3);
%     P2(1:nmax,:) = P;
    tet2 = NaN(nele,10);
    
    
%     faceInd = [1,4,2,8,10,5;
%                1,3,4,9,7,8;
%                2,4,3,10,7,6;
%                1,2,3,5,6,9];
    
    faceInd = [1,4,2,8,9,5;
               1,3,4,7,10,8;
               3,2,4,6,9,10;
               1,2,3,5,6,7];
           
%     faceInd = [1,4,2,8,5,9;
%                1,4,3,7,8,10;
%                3,4,2,6,10,9;
%                1,3,2,5,7,6];
    

%     tet2(:,1:4) = tet;
    edgesl = [1,2;2,3;3,1;1,4;2,4;3,4];
%     edgesl = [1,2;2,3;4,3;1,4;1,3;2,4];
    edges = NaN(6*nele,2);
    newEdges = edges;
    neighs = TR.neighbors;
    
    c = 1;
    for iel = 1:nele
       iv = tet(iel,:);
       
       [iedgesn, ivn2] = NeighborData(iel,tet2,neighs,edgesl);
       iedges = sort(iv(edgesl),2);
       
       currEdges = zeros(6,2);
       iv2 = [0,0,0,0,0,0];
       for ied=1:6
           existsNeighborEdges = ~isempty(iedgesn);
           if existsNeighborEdges
               k = iedges(ied,1)==iedgesn(:,1) & iedges(ied,2)==iedgesn(:,2);
               if any(k)
                   ind = find(k,1);
                   iv2(ied)=ivn2(ind);
                   currEdges(ied,:) = iedgesn(ind,:);
%                    edges(c,:) = iedgesn(ind,:);
               else
%                    edges(c,:) = iedges(ied,:);
                   currEdges(ied,:) = iedges(ied,:);
                   newEdges(c,:) = iedges(ied,:);
                   iv2(ied)=nnod+c;
                   c = c+1;
               end
           else
%                edges(c,:) = iedges(ied,:);
               currEdges(ied,:) = iedges(ied,:);
               newEdges(c,:) = iedges(ied,:);
               iv2(ied)=nnod+c;
               c = c+1;
           end
           
       end
       ivv = [iv,iv2]; 
       tet2(iel,:) = ivv;
       
       
       

       
       Elements(iel).edges = [currEdges,iv2'];
       Elements(iel).neighs = neighs(iel,:);
    end
    newEdges = newEdges(~all(isnan(newEdges),2),:);
    Pnew = (P(newEdges(:,1),:)+P(newEdges(:,2),:))/2;
    P2 = [P;Pnew];
    
    %%
%     xfigure; light
%     for iel = 1:nele
%         
%         ivv = tet2(iel,:);
%         faces3 = Elements(iel).faces(:,1:3);
%         faces6 = Elements(iel).faces
%         hp = patch('Faces',faces3,'Vertices',P2,'FaceColor','none');
%         text(P2(ivv,1),P2(ivv,2),P2(ivv,3),cellstr(num2str( (ivv)' )), 'BackgroundColor', 'w')
%         Elements(iel).edges
%         1;
%     end
    
end

function [iedgesn, ivn2] = NeighborData(iel,tet2,neighs,edgesl)
    neighEle1 = neighs(iel,~isnan(neighs(iel,:)));
    neighEle2 = unique(neighs(neighEle1,:));
    neighEle2 = neighEle2(~isnan(neighEle2));
    neighEle = unique(setdiff([neighEle1(:);neighEle2(:)],iel));

    ivn = tet2(neighEle,1:4);
    ivn2 = tet2(neighEle,5:end);
    nind = ~any(isnan(ivn),2);
    ivn = ivn(nind,:);
    ivn2 = ivn2(nind,:);
    ivn2 = ivn2.';
    ivn2 = ivn2(:);
    nneigh = size(ivn,1);
    iedgesn = zeros(nneigh*6,2);
    c2 = 1;
    for i = 1:6:nneigh*6
        ivni = ivn(c2,:);
        iedgesn(i:i+5,:) = sort(ivni(edgesl),2);
        c2 = c2+1;
    end
end
