function h = vizP1Mesh(T,P)

    tri = [T(:,[1,2,3]); T(:,[1,2,4]); T(:,[1,3,4]); T(:,[2,4,3])];
    h.patch = patch('Faces',tri,'Vertices',P,'FaceColor','c','FaceAlpha',1);
    xlabel('X'); ylabel('Y'); axis equal;
    axis(axispad([min(P(:,1)),max(P(:,1)),min(P(:,2)),max(P(:,2))],0.1));
    
%     for i = 1:size(P,1)
%         text(P(i,1),P(i,2),P(i,3),num2str(i),'BackgroundColor','w')
%     end
%     for i = 1:size(T,1)
%         xm = mean(xnod(T(i,:)));
%         ym = mean(ynod(T(i,:)));
%         zm = mean(znod(T(i,:)));
%         text(xm,ym,zm,num2str(i),'BackgroundColor','y')
%     end


end


function X = axispad(X,p)
    X(1:2:end) = X(1:2:end)-p;
    X(2:2:end) = X(2:2:end)+p;
end