function h = vizP2Faces(tri,P,np,varargin)
    % h = vizP2Faces(tri,P,nt,properties)
    %
    % tri               - Triangulatin P2
    % P                 - Points
    % nt                - Number of triangles per P2 patch
    %
    % properties:
    % 'FaceColor'       - Default is 'w'
    % 'FaceLighting'    - Default is 'gouraud'
    % 'EdgeColor'       - Default is 'k'
    % 'InnerEdgeColor'  - Default is [1,1,1]*0.4)
    % 'EdgeLineWidth'   - Default is 1.5
    % 'Light'           - Default is 'false'
    

    nP2ele=size(tri,1);
    nTriEle = nP2ele*np^2;
    if nTriEle>30000
        warning('PLOTP2:manyelements',['Number of generates triangular elements is too damn high (',num2str(nTriEle),')!'])
    end
    
    if np > 1
        dx=1/np;
        [X,Y] = meshgrid(0:dx:1,0:dx:1);
        x=X(:);y=Y(:);
        ind=find(y>eps+1-x);
        x(ind)=[];y(ind)=[];

        tt = subTriang(np,x);

        T = zeros(nP2ele*np^2,3);
        X = zeros(nP2ele*length(x)^2,3);
        kk = zeros(nP2ele,3*(np+1)-2);

        for iel=1:nP2ele
            iv6=tri(iel,:);
    %         iv3=tri(iel,1:3);
            xc=P(iv6,1);yc=P(iv6,2);zc=P(iv6,3);

            fiP2=[1-3*x+2*x.^2-3*y+4*x.*y+2*y.^2,x.*(-1+2*x),y.*(-1+2*y),-4*x.*(-1+x+y),4*x.*y,-4*y.*(-1+x+y)];
    %         fiP1=[1-x-y,x,y];

            xn=fiP2*xc;
            yn=fiP2*yc;
            zn=fiP2*zc;

            % Insert coordinates from iel into X, that will contain all coordinates
            lx=length(xn);
            if iel==1
                X(iel:lx,:)=[xn,yn,zn];
            else
                X((iel-1)*lx+1:iel*lx,:)=[xn,yn,zn];
            end

    %         tt = subTriang(np,x);

            % Global triangle matrix is assembled
            lt=size(tt,1);
            if iel==1
                T(iel:lt,:)=tt;
                tt0=tt;
                tt1=tt0;
            else
                maxtt=max(tt(:));
                tt1=tt1+maxtt;
                lt=size(tt1,1);
                T((iel-1)*lt+1:iel*lt,:)=tt1;
            end

            % Global element edge vector is assembled
            % this is needed to draw the P2 element edges
            indx=find(x == min(x));
            indy=find(y == min(y));
            ns=np+1;
            ns3=zeros(1,ns);
            ns3(1,1)=ns;
            ii=2;
            for i1=ns-1:-1:1
                ns3(1,ii)=ns3(1,ii-1)+i1;
                ii=ii+1;
            end
            k = [indx;ns3(2:end)';indy(end-1:-1:1)];

            k1 = k'+max(kk(:));
            kk(iel,:) = k1;

        end
    else
        T = tri(:,1:3);
%         T = [tri(:,[1,2,3]); tri(:,[1,2,4]); tri(:,[1,3,4]); tri(:,[2,4,3])];
        X = P;
    end
    
    %% Input parser
    p = inputParser;
    addParameter(p,'FaceColor','w');
    addParameter(p,'FaceLighting','gouraud');
    addParameter(p,'EdgeColor','k');
    addParameter(p,'InnerEdgeColor',[1,1,1]*0.4);
    addParameter(p,'EdgeLineWidth',1.5);
    addParameter(p,'Light',false);
    parse(p,varargin{:});
    PR = p.Results;
    
    %% Draw Graphics
    h.patch =  patch('faces',T,'vertices',X,'FaceColor', PR.FaceColor);
    set(h.patch,'FaceLighting', PR.FaceLighting)
    if PR.Light
        hca = gca;
        lightexist = 0;
        for i = 1:length(hca.Children)
            if isa(hca.Children(i), 'matlab.graphics.primitive.Light')
               lightexist = 1; 
            end
        end
        if ~lightexist
            h.light = light;
        end
    end
    axis equal; hold on;
    
    if np > 1
        h.patch.EdgeColor = PR.InnerEdgeColor;
        h.edge=trimesh(kk,X(:,1),X(:,2),X(:,3));
        set(h.edge,'FaceColor','none')
        set(h.edge,'EdgeColor', PR.EdgeColor)
        set(h.edge,'EdgeLighting','none','LineWidth', PR.EdgeLineWidth)
    else
        h.patch.EdgeColor = PR.EdgeColor;
        h.patch.LineWidth = PR.EdgeLineWidth;
    end
    
end

function tt = subTriang(n,x)
    nT=length(x);
    ns=n+1;


    ns3=zeros(1,ns);
    ns3(1,1)=ns;
    ii=2;
    for i1=ns-1:-1:1
        ns3(1,ii)=ns3(1,ii-1)+i1;
        ii=ii+1;
    end
    % ns3; %Third side T

    emax=-1;
    for i1=1:n
        emax=emax+2;
    end
    % emax; %number T in last sector
    elements=1:2:emax;
    nelem = sum(elements); %number elements
    T1 = 1:nT; %number T

    si=1;
    ei=ns;
    T2=zeros(ns);
    for i1=1:ns
        T2(i1,i1:ns) = T1(si:ei);
        si=ei+1;
        ei=ns3(i1)+ns-i1;
    end

    tri2=zeros(nelem,3);
    i1=1;
    giel=1;
    for iseq=elements
        if iseq==1
            seq1 = T2(:,[1,2]);
            ind1= seq1==0;
            seq1(ind1)=[];
            seq1 = [seq1(3),seq1(2),seq1(1)];
            tri2(1,:)=seq1;
            giel=2;
        end
        if iseq~=1
            seqi = T2(:,[i1,i1+1]);
            seqi1=seqi(:,1);
            seqi2=seqi(:,2);
            ind1= seqi1==0;
            seqi1(ind1)=[];
            c1=length(seqi1);

            ind2= seqi2==0;
            seqi2(ind2)=[];
            c2=length(seqi2);

            seqi=seqi(:);
            ind1= seqi==0;
            seqi(ind1)=[];
            seqi = seqi';
            a = c2-1;
            b = iseq - a;
            for ai=1:a
                tri2(giel,:) = seqi([ai,ai+c1+1,ai+c1]);
                giel=giel+1;
            end
            for bi=1:b
                tri2(giel,:) = seqi([bi,bi+1,bi+c1+1]);
                giel=giel+1;
            end
        end
        i1=i1+1;
    end
    tt=tri2;
end