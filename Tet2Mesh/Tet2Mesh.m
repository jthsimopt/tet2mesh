classdef Tet2Mesh
    %Tet2Mesh Second order tetrahedral mesh class
    %   Creates a background mesh that is used for unfitted finite element
    %   methods, e.g., CutFEM
    
    
    properties
        T
        P
        faces
        edges
        Elements
        neighs
        nele
        nnod
    end
    
    methods
        function O = Tet2Mesh(x0,x1,y0,y1,z0,z1,nxe,nye,nze)
            [T,P,~,~] = HexMesh(x0,x1,y0,y1,z0,z1,nxe,nye,nze);
            tet = FishTri3D(T,P,nxe,nye,nze);
            [tet2,P2,Elements] = Convert2Tet2(tet,P);
            

            nele = size(tet2,1);
            [Elements, facesP2] = AssembleFaces(tet2,Elements);
            
            TR = triangulation(tet,P);
            
            T = tet2;
            edges = [T(:,1),T(:,5),T(:,2);
                     T(:,2),T(:,6),T(:,3);
                     T(:,3),T(:,7),T(:,1);
                     T(:,1),T(:,8),T(:,4);
                     T(:,2),T(:,9),T(:,4);
                     T(:,3),T(:,10),T(:,4)];
            edgess = sort(edges(:,[1,3]),2);
            [~,id] = unique(edgess,'rows');
            O.edges = [edgess(id,1),edges(id,2),edgess(id,2)];
            
            O.neighs = TR.neighbors;
            O.nele = nele;
            O.nnod = size(P2,1);
            O.T = tet2;
            O.P = P2;
            O.Elements = Elements;
            O.faces = facesP2;
            
        end
        
        function faces = tet2Faces(O,ele)
            nele = length(ele);
            faces = zeros(4*nele,6);
            allFaces = O.faces;
            
            lo = 1;
            for iel = ele'
                up = lo+4-1;
                faces(lo:up,:) = allFaces(iel*4-3:iel*4,:);
                lo = up+1;
            end
            
            1;
        end
        
    end    
end

function [Elements, facesP2] = AssembleFaces(tet2,Elements)

    faceInd = [1,4,2,8,9,5;
               1,3,4,7,10,8;
               3,2,4,6,9,10;
               1,2,3,5,6,7];
    % faceInd = [1,4,2,8,5,9;
    %            1,4,3,7,8,10;
    %            3,4,2,6,10,9;
    %            1,3,2,5,7,6];
    
    nele = size(tet2,1);
    facesP2 = zeros(4*nele,6);
    lo = 1;
    for iel = 1:nele
        iv = tet2(iel,:);
        ifaces = iv(faceInd);
%         ifaces3 = ifaces(:,1:3);
%         xfigure(1); light
%         hp = patch('Faces',ifaces3,'Vertices',P,'FaceColor','none');
%         text(P(iv,1),P(iv,2),P(iv,3),cellstr(num2str( (iv)' )), 'BackgroundColor', 'w')
%         
        
        if iel == 1
            Elements(iel).faces = ifaces;
        else
            ineighs = Elements(iel).neighs;
            ineighs = ineighs(Elements(iel).neighs < iel);
            for ieln = ineighs
                ifacesn = Elements(ieln).faces;
                ifaceMap = all(ismember(ifaces,ifacesn),2);
                ifacenMap = all(ismember(ifacesn,ifaces),2);
                ifaces(ifaceMap,:) = ifacesn(ifacenMap,:);
                1;
            end
            Elements(iel).faces = ifaces;
        end
        
        up = lo+4-1;
        facesP2(lo:up,:) = ifaces;
        lo = up+1;
        
        1;
    end
    
end