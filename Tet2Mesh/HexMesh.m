function [T,P,Faces,edges] = HexMesh(x0,x1,y0,y1,z0,z1,nxe,nye,nze)
    %   8-----7
    %  /|    /|
    % 6-----5 |
    % | 4...|.3
    % |/    |/ 
    % 2-----1
    
    x = linspace(x0,x1,nxe+1);
    y = linspace(y0,y1,nye+1);
    z = linspace(z0,z1,nze+1);
    [MX, MY, MZ] = meshgrid(x,y,z);

    P = [MX(:),MY(:),MZ(:)];

    nx = nxe+1;
    ny = nye+1;
    nz = nze+1;

    nele = nxe*nye*nze;

    nodes = zeros(nele, 8);
    c = 1;
    iel = 1;
    for k = 1:nz
        for j = 1:nx
            for i = 1:ny

                if i ~= ny && j ~=nx && k ~= nz
                    nodes(iel,:) = [c,c+1,c+ny+1,c+ny,c+nx*ny,c+nx*ny+ny,c+nx*ny+ny+1,c+nx*ny+1];
                    iel = iel + 1;
                end
                c = c+1;
            end
        end
    end

    O.nx = nx;
    O.ny = ny;
    O.nz = nz;

    %             T.Connectivity = nodes;


    Q = zeros(nele*6,4);
    edges1 = zeros(nele*12,2);

    Element(nele).faces = [];
    Element(nele).edges = [];

    lof = 1; loe = 1;
    for iel = 1:nele
        upf = lof+5;
        upe = loe+11;
        %Faces
        iface = [nodes(iel,[1,2,3,4]);...
            nodes(iel,[5,6,7,8]);...
            nodes(iel,[1,5,8,2]);...
            nodes(iel,[4,6,5,1]);...
            nodes(iel,[3,7,6,4]);...
            nodes(iel,[2,8,7,3])];
        Q(lof:upf,:) = iface;
        O.Element(iel).faces = iface;

        %edges
        n = nodes(iel,:);
        I = [1     2     4     3     5     8     6     7];
        iedges = [n(I(1)),n(I(2));...
            n(I(2)),n(I(4));...
            n(I(4)),n(I(3));...
            n(I(3)),n(I(1));...
            n(I(1)),n(I(5));...
            n(I(3)),n(I(7));...
            n(I(4)),n(I(8));...
            n(I(2)),n(I(6));...
            n(I(6)),n(I(5));...
            n(I(5)),n(I(7));...
            n(I(7)),n(I(8));...
            n(I(8)),n(I(6))];
        edges1(loe:upe,:) = iedges;
        Element(iel).edges = iedges;

        T = nodes(:,[1,2,3,4,5,6,7,8]);
        %counter
        lof = upf +1;
        loe = upe +1;


    end

    Faces = Q;

%     nnod = length(P);
    edges = edges1;

end