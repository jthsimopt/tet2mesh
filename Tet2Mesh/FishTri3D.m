function T2 = FishTri3D(nodes,P,nxe,nye,nze)
    T = nodes;
    nele = size(T,1);
    T2 = zeros(6*nele,4);
    iel = 1;
    for k = 1:nze
        for j = 1:nxe
            for i = 1:nye
                forwardx = logical(mod(j,2));
                forwardy = logical(mod(i,2));
%                 forwardz = logical(mod(k,2));
                forwardz = 1;
                forwardxy = forwardx == forwardy;
                forward = forwardxy == forwardz;
                if forward
                    tri1 = T(iel,[1,2,3,8]);
                    tri2 = T(iel,[1,5,8,3]);
                    tri3 = T(iel,[3,7,8,5]);
                    tri4 = T(iel,[1,4,6,3]);
                    tri5 = T(iel,[1,5,6,3]);
                    tri6 = T(iel,[5,6,7,3]);
                else
                    tri1 = T(iel,[2,1,5,4]);
                    tri2 = T(iel,[2,8,5,4]);
                    tri3 = T(iel,[8,5,6,4]);
                    tri4 = T(iel,[4,3,7,2]);
                    tri5 = T(iel,[7,2,8,4]);
                    tri6 = T(iel,[7,8,6,4]);
                end
                T2(6*iel-5,:) = tri1;
                T2(6*iel-4,:) = tri2;
                T2(6*iel-3,:) = tri3;
                T2(6*iel-2,:) = tri4;
                T2(6*iel-1,:) = tri5;
                T2(6*iel-0,:) = tri6;
                            
%                             xfigure(554); axis equal; view(3)
%                             drawElement(tri1,P,'c')
%                             drawElement(tri2,P,'c')
%                             drawElement(tri3,P,'c')
%                             drawElement(tri4,P,'c')
%                             drawElement(tri5,P,'c')
%                             drawElement(tri6,P,'c')

                iel=iel+1;
            end
        end
    end
end

function drawElement(itet,P,color)
    faceInd = [1,4,2;
               1,3,4;
               3,2,4;
               1,2,3;];
    faces = itet(faceInd);
    hp = patch('Faces',faces,'Vertices',P,'FaceColor',color);
    hp.FaceAlpha = 0.1;
end